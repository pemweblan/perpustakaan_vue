import { createRouter, createWebHistory } from 'vue-router'

const routes = [
    {
        path: '/',
        name: 'buku.index',
        component: () => import( /* webpackChunkName: "post.index" */ '@/views/buku/BukuIndex.vue')
    },
    {
        path: '/create',
        name: 'buku.create',
        component: () => import( /* webpackChunkName: "post.create" */ '@/views/buku/BukuCreate.vue')
    },
    {
        path: '/edit',
        name: 'buku.edit',
        component: () => import( /* webpackChunkName: "post.create" */ '@/views/buku/BukuEdit.vue')
    }
]

//create router
const router = createRouter({
    history: createWebHistory(),
    routes  // config routes
})

export default router